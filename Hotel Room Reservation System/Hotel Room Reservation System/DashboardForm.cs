﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel_Room_Reservation_System
{
    public partial class DashboardForm : Form
    {

        private AddRoomForm addRoomForm = new AddRoomForm();
        public DashboardForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            addRoomForm.Show();
        }
    }
}
